<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate($from, $to, $unit = 'km') {
		$lat1 = $from[0];
		$lon1 = $from[1];

		$lat2 = $to[0];
		$lon2 = $to[1];

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;


        if ($unit == "km") {
            return ($miles * 1.609344);
        } else if ($unit == "m") {
            return (($miles * 1.609344) * 1000);
        } else {
            return 'Wrong unit';
        }

	}

	/**
	 * @param array $from
	 * @param array $offices
	 *
	 * @return array
	 */
	public function findClosestOffice($offices) {

		return min($offices);
	}

}