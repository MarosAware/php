<?php
/**
 * Created by PhpStorm.
 * User: marosaware
 * Date: 08.09.18
 * Time: 12:23
 */

namespace App\Http\Controllers;


use App\Http\Services\OpeningHoursService;
use Proexe\BookingApp\Bookings\Models\BookingModel;
use Spatie\OpeningHours\OpeningHours;

class TestingController extends Controller
{
    public function test(OpeningHoursService $hoursService)
    {
        $data = $hoursService->prepareData();

        foreach ($data as $week) {

            dump($week);
            $openingHours = OpeningHours::create($week);

            dump($openingHours->forWeek());
        }

    }
}