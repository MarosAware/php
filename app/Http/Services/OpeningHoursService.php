<?php
/**
 * Created by PhpStorm.
 * User: marosaware
 * Date: 08.09.18
 * Time: 12:47
 */

namespace App\Http\Services;
use Proexe\BookingApp\Bookings\Models\BookingModel;
use Spatie\OpeningHours\OpeningHours as Spatie;


class OpeningHoursService
{

    public function prepareData()
    {
        $days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
        ];

        $bookings = BookingModel::with('office')->get()->toArray();

        foreach ($bookings as $booking) {
            $j = 0;
            $week = [];
            foreach($booking['office']['office_hours'] as $day) {

                if (!key_exists('from', $day)) {
                    $week[$days[$j]] = [];
                    $j++;
                    continue;
                }
                $week[$days[$j]] = [$day['from'] . '-' . $day['to']];
                $j++;
            }
            $data[] = $week;

        }

        return $data;

    }



    public function getOpeningWeek($week)
    {
        $openingHours = Spatie::create($week);

        return $openingHours->forWeek();
    }
}